import React, {Component} from 'react';
import Demo from "../../components/Demo/Demo";
import Modal from "../../components/UI/Modal/Modal";
import Button from "../../components/UI/Button/Button";
import Alert from "../../components/UI/Alert/Alert";
import Header from "../../components/Demo/Header/Header";
import Footer from "../../components/Demo/Footer/Footer";

class MyDemo extends Component {
	
	state = {
		modalShow: false,
		alertShow: false
	};
	
	closed = () => {
		this.setState({modalShow: false});
	};
	
	open = () => {
		this.setState({modalShow: true});
	};
	
	continued = () => {
		this.setState({alertShow: true});
	};
	
	closedAlert = () => {
		this.setState({alertShow: false});
	};
	
	buttons = [
		{type: 'primary', label: 'Continue', clicked: this.continued},
		{type: 'danger', label: 'Close', clicked: this.closed}
	];
	
	render() {
		return (
			<div className='main'>
				<Modal
					show={this.state.modalShow}
					closed={this.closed}
					title="Some kinda modal title"
				>
					<p>This is modal content</p>
					{
						this.buttons.map(button => {
							return <Button
								key={button.type} btnType={button.type}
								clicked={button.clicked}>{button.label}
							</Button>
						})
					}
				</Modal>
				<Header/>
				<Demo clcked={this.open}/>
				<Footer/>
				<Alert
					show={this.state.alertShow}
					type="danger"
					clickDismissable
					dismiss={this.closedAlert}
				>This is a warning type alert</Alert>
			</div>
		)
	}
}

export default MyDemo;