import React from 'react';
import CloseButton from "../CloseButton/CloseButton";
import './Alert.css'

const Alert = props => {
	return (
		<div
			className={['Alert', props.type].join(' ')}
			style={{display: props.show ? 'block' : 'none'}}
			onClick={props.clickDismissable ? props.dismiss : null}
		>
			<CloseButton show={props.clickDismissable} clicked={props.dismiss}/>
			{props.children}
		</div>
	)
};

export default Alert;