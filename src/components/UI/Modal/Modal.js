import React from 'react';
import './Modal.css';
import Wrapper from "../../../hoc/Wrapper";
import Backdrop from "../Backdrop/Backdrop";
import CloseButton from "../CloseButton/CloseButton";

const Modal = props => {
	return (
		<Wrapper>
			<Backdrop show={props.show} clicked={props.closed}/>
			<div className="Modal" style={{
				transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
				opacity: props.show ? '1' : '0'
			}}>
				<CloseButton clicked={props.closed}/>
				<div className="title">{props.title}</div>
				{props.children}
			</div>
		</Wrapper>
	)
};

export default Modal;