import React from 'react';
import './CloseButton.css'

const CloseButton = props => {
	return <span
		style={{display: props.show ? 'none' : 'block'}}
		onClick={props.clicked}
		className="CloseButton">
			<i className="fas fa-times-circle"></i>
		</span>
};

export default CloseButton;